# esiPressureGauge conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/esiPressureGauge.git/esiPressureGauge"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS esiPressureGauge module
